<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\User\{AuthController, UserController};
use \App\Http\Controllers\Api\v1\Product\ProductController;
use \App\Http\Controllers\Api\v1\Vehicle\VehicleController;
use \App\Http\Controllers\Api\v1\Payment\PaymentController;
use \App\Http\Controllers\Api\v1\Notification\NotificationController;
use \App\Http\Controllers\Api\v1\Card\CardController;
use \App\Http\Controllers\Api\v1\Order\OrderController;
use \App\Http\Controllers\Api\v1\Client\ClientController;
use \App\Http\Controllers\Api\v1\City\CityController;
use \App\Http\Controllers\Api\v1\Faq\FaqController;
use \App\Http\Controllers\Api\v1\Promo\PromoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Auth routes
 */
Route::group(
    ['namespace'  => 'Api/v1', 'prefix'  => 'kassa-api/v1', 'middleware' => 'api'],
    function () {
        Route::post('login', [AuthController::class, 'login']);
    }
);

Route::group(['namespace'  => 'Api/v1', 'prefix'  => 'kassa-api/v1', 'middleware' => ['api', 'auth']], function () {

    Route::group(['namespace'  => 'Products', 'prefix'  => 'products'], function () {
        Route::get('/grocery/list/{id}', [ProductController::class, 'groceryProducts']);
        Route::get('/kassa/list/{id}', [ProductController::class, 'kassaProducts']);
    });

    Route::group(['namespace'  => 'Order', 'prefix'  => 'order'], function () {
        Route::post('/create', [OrderController::class, 'create']);
        Route::get('/orders', [OrderController::class, 'orders']);
        Route::patch('/new', [OrderController::class, 'new']);
        Route::patch('/in-progress', [OrderController::class, 'inProgress']);
        Route::patch('/completed', [OrderController::class, 'completed']);
    });

    Route::group(['namespace'  => 'Client', 'prefix'  => 'client'], function () {
        Route::get('/info', [ClientController::class, 'info']);
    });
});


