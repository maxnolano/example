<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Throwable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {

        $code = null;
        $message = $e->getMessage();
        $trace = $e->getTraceAsString();

        Log::error('Необработанное исключение: ' . $message.' '.$trace);

        //print_r($e);

        if ($e instanceof ModelNotFoundException || $e instanceof NotFoundHttpException) {
            $code = 404;
            $message = 'Не найдено';
        } elseif ($e instanceof AuthorizationException || $e instanceof AuthenticationException) {
            $code = 401;
            $message = 'Требуется авторизация';
        } elseif ($e instanceof ValidationException) {
            $code = 422;
            $message = $e->getMessage();
        }

        if (!$code) {
            $code = 500;
            $message = "Сервис временно не доступен";
        }

        return response()
            ->json([
                'message' => $message,
                'status' => 0
            ])
            ->setStatusCode($code);
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json(['message' => $exception->getMessage()], 401);
    }
}
