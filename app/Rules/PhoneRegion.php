<?php


namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneRegion implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (substr($value, 0, 2) !== '77')
            return false;

        return true;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Ваш регион не поддерживается в приложений';
    }
}