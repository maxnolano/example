<?php


namespace App\Rules;


use Illuminate\Contracts\Validation\Rule;

class Phone implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if(strlen($value) !== 11)
            return false;

        return true;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Необходимо ввести телефон в формате 77000000000';
    }

}
