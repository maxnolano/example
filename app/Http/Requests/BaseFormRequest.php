<?php


namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class BaseFormRequest   extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        //parent::failedValidation($validator);
        //throw new ValidationException($validator);

        response()->json([
            'message' => $validator->errors()->first(),
            'status' => 0
        ], 422)->send();
        exit();

        //throw ValidationException::withMessages([$validator->errors()->first()]);
       /* throw new ValidationException(
            response()->json([
                'status' => 0,
                'message' => $validator->errors()->first()
            ], 200)
        );*/
    }
}