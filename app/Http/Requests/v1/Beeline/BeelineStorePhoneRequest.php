<?php


namespace App\Http\Requests\v1\Beeline;


use App\Http\Requests\BaseFormRequest;
use App\Rules\Phone;
use App\Rules\PhoneRegion;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Factory as ValidationFactory;

class BeelineStorePhoneRequest extends BaseFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'phone' => ['required', 'string', 'unique:crm_beeline_phones,phone_number', new Phone, new PhoneRegion],
            'phone' => ['required', 'string'],
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'Телефон обязателен для заполнения',
            'phone.string' => 'Телефон должен быть строкой',
        ];
    }

}
