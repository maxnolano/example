<?php


namespace App\Http\Requests\v1\Beeline;


use App\Http\Requests\BaseFormRequest;

class BeelineDestroyPhoneRequest extends BaseFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_id' => ['required', 'integer', 'exists:crm_beeline_phones,id'],
        ];
    }

    public function messages()
    {
        return [
            'phone_id.required' => 'Телефон обязателен для заполнения',
            'phone_id.integer' => 'Телефон должен быть числом',
            'phone_id.exists' => 'Такого телефона нет в базе',
        ];
    }
}
