<?php

namespace App\Http\Requests\v1\Order;

use App\Http\Requests\BaseFormRequest;

class ListOrdersRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kassa_status' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'kassa_status.required' => 'kassa_status обязателен для заполнения'
        ];
    }
}
