<?php

namespace App\Http\Requests\v1\Order;

use App\Http\Requests\BaseFormRequest;

class OrderCreateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cashier_id' => ['required'],
            'products' => ['required', 'array'],
            'order_for' => ['required'],
            'total_order_amt' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'cashier_id.required' => 'cashier_id обязателен для заполнения',
            'products.required' => 'products обязателен для заполнения',
            'order_for.required' => 'order_for обязателен для заполнения',
            'total_order_amt.required' => 'total_order_amt обязателен для заполнения',
        ];
    }
}
