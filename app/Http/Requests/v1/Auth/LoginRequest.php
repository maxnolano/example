<?php

namespace App\Http\Requests\v1\Auth;

use App\Http\Requests\BaseFormRequest;
use App\Rules\{Phone, PhoneRegion};

class LoginRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usr' => ['required', 'string'],
            'pwd' => ['required', 'string'],
        ];
    }

    public function messages()
    {
        return [
            'usr.required' => 'Телефон обязателен для заполнения',
            'usr.string' => 'Телефон должен быть строкой',
        ];
    }
}
