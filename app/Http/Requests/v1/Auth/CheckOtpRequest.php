<?php

namespace App\Http\Requests\v1\Auth;

use App\Http\Requests\BaseFormRequest;
use App\Rules\Phone;
use App\Rules\PhoneRegion;

class CheckOtpRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => ['required', 'string', new Phone, new PhoneRegion],
            'otp' => ['required', 'numeric', 'digits:6'],
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'Телефон обязателен для заполнения',
            'phone.string' => 'Телефон должен быть строкой',
            'otp.required' => 'Смс код обязателен для заполнения',
            'otp.numeric' => 'Смс код должен быть числом',
            'otp.digits' => 'Смс код должен быть :value символов',
        ];
    }
}
