<?php

namespace App\Http\Requests\v1\User;

use App\Http\Requests\BaseFormRequest;
//use App\Rules\{Phone, PhoneRegion};

class ProfileImageRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_name' => ['required', 'image'], //|mimes:png,jpg|max:2048
        ];
    }

    public function messages()
    {
        return [
            'file_name.required' => 'Файл обязателен для заполнения',
        ];
    }
}
