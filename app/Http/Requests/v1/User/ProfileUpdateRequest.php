<?php

namespace App\Http\Requests\v1\User;

use App\Http\Requests\BaseFormRequest;
//use App\Rules\{Phone, PhoneRegion};

class ProfileUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => ['nullable'],
            'first_name' => ['nullable'],
            'last_name' => ['nullable'],
            'vehicle_plate_number' => ['nullable'],
            'vehicle_model_id' => ['nullable'],
            'vehicle_company_id' => ['nullable'],
            'email' => ['nullable'],
            'city_id' => ['nullable'],
            'iin' => ['nullable'],
        ];
    }

    public function messages()
    {
        return [
            'display_name.required' => 'Имя обязателен для заполнения',
            //'phone.string' => 'Телефон должен быть строкой',
            'plate_number.required' => 'Номер авто обязателен для заполнения',
        ];
    }
}
