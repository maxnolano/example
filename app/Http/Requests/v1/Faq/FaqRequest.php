<?php

namespace App\Http\Requests\v1\Faq;

use App\Http\Requests\BaseFormRequest;
//use App\Rules\{Phone, PhoneRegion};

class FaqRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'app_language' => ['required'], //|mimes:png,jpg|max:2048
        ];
    }

    public function messages()
    {
        return [
            //'token_id.required' => 'обязателен для заполнения',
        ];
    }
}
