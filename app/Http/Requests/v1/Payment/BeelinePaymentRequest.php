<?php


namespace App\Http\Requests\v1\Payment;


use App\Http\Requests\BaseFormRequest;

class BeelinePaymentRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_id' => ['required', 'integer', 'exists:crm_beeline_phones,id'],
            'store_id' => ['required', 'integer', 'exists:crm_stores,id'],
            'cashier_id' => ['required', 'integer'],
            'pump_id' => ['required', 'integer', 'exists:crm_pumps,pump_id'],
            'store_side' => ['required', 'integer'],
            'order_amount' => ['required', 'integer'],
            'order_type' => ['required', 'integer'],
            'pump_number' => ['required', 'integer'],
        ];
    }

    public function messages()
    {
        return [
            'phone_id.required' => 'Телефон обязателен для заполнения',
            'phone_id.integer' => 'Телефон должен быть числом',
            'phone_id.exists' => 'Такого телефона нет в базе',
            'store_id.required' => 'Магазин обязателен для заполнения',
            'store_id.integer' => 'Магазин должен быть числом',
            'store_id.exists' => 'Такого магазина нет в базе',
            'cashier_id.required' => 'Касса обязательна для заполнения',
            'cashier_id.integer' => 'Касса должен быть числом',
            'pump_id.required' => 'Номер колонки обязателен для заполнения',
            'pump_id.integer' => 'Номер колонки должен быть числом',
            'pump_id.exists' => 'Такой колонки нет в базе',
            'store_side.required' => 'store_side required',
            'store_side.integer' => 'store_side must be int',
            'order_amount.required' => 'Сумма обязательна для заполнения',
            'order_amount.integer' => 'Сумма должна быть числом',
            'order_type.required' => 'Тип заказа обязательна для заполнения',
            'order_type.integer' => 'Тип заказа должен быть числом',
            'pump_number.required' => 'pump_number required',
            'pump_number.integer' => 'pump_number must be int',
        ];
    }
}
