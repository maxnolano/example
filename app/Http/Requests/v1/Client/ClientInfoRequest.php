<?php

namespace App\Http\Requests\v1\Client;

use App\Http\Requests\BaseFormRequest;

class ClientInfoRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'uuid.required' => 'uuid обязателен для заполнения'
        ];
    }
}
