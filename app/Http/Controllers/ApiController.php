<?php


namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{

    /**
     *
     * @OA\Info(
     *      version="0.0.0",
     *      title="Smartgas Api",
     *      description="Описание API проекта Smartgas",
     * )
     * @OA\SecurityScheme(
     *      in="header",
     *      securityScheme="ApiKey",
     *      type="http",
     *      scheme="bearer",
     * ),
     *
     * @OA\Server(
     *          url="http://185.22.64.100:81/mobile-api",
     *          description="Сервер для разработки"
     *      )
     * @OA\Server(
     *          url="http://smartgas.develop/mobile-api",
     *          description="Локальный сервер"
     *      )
     *
     *
     */


    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function result($data)
    {
        return response()->json($data['data'])->setStatusCode($data['httpCode']);
    }
}
