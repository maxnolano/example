<?php

namespace App\Http\Controllers\Api\v1\Product;

use App\Http\Controllers\ApiController;
use App\Services\v1\Product\GroceryStationService;
use App\Services\v1\Product\KassaStationService;
use Illuminate\Http\JsonResponse;


class ProductController extends ApiController
{
    public function groceryProducts(int $id): JsonResponse
    {
        $response = (new GroceryStationService($id))->make();
        return $this->result($response);
    }

    public function kassaProducts(int $id): JsonResponse
    {
        $response = (new KassaStationService($id))->make();
        return $this->result($response);
    }
}

