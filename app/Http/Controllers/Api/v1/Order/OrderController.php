<?php

namespace App\Http\Controllers\Api\v1\Order;

use App\Http\Controllers\ApiController;
use App\Services\v1\Order\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\v1\Order\OrderCreateRequest;
use App\Http\Requests\v1\Order\ListOrdersRequest;
use App\Http\Requests\v1\Order\ChangeStatusRequest;

class OrderController extends ApiController
{
    /**
     * Create order
     * @param Request $request
     * @return Object
     */
    public function create(OrderCreateRequest $request): JsonResponse
    {
        $response = (new OrderService($request))->createOrder();
        return $this->result($response);
    }

    /**
     * Get list of orders (new, in_progress, ready)
     * @param Request $request
     * @return Object
     */
    public function orders(ListOrdersRequest $request): JsonResponse
    {
        $response = (new OrderService($request))->orders();
        return $this->result($response);
    }

    /**
     * Change order status to new order
     * @param Request $request
     * @return Object
     */
    public function new(ChangeStatusRequest $request): JsonResponse
    {
        $response = (new OrderService($request))->new();
        return $this->result($response);
    }
    
    /**
     * Change order status to in_progress
     * @param Request $request
     * @return Object
     */
    public function inProgress(ChangeStatusRequest $request): JsonResponse
    {
        $response = (new OrderService($request))->inProgress();
        return $this->result($response);
    }

    /**
     * Change order status to completed
     * @param Request $request
     * @return Object
     */
    public function completed(ChangeStatusRequest $request): JsonResponse
    {
        $response = (new OrderService($request))->completed();
        return $this->result($response);
    }
}

