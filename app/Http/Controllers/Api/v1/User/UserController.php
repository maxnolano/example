<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Helper\ImageHelper;
use App\Http\Controllers\ApiController;
use App\Http\Requests\v1\User\DeviceTokenRequest;
use App\Http\Requests\v1\User\GroupMemberRequest;
use App\Http\Requests\v1\User\GroupRequest;
use App\Http\Requests\v1\User\ProfileImageRequest;
use App\Http\Requests\v1\User\ProfileUpdateRequest;
use App\Services\v1\User\{DeviceTokenService,
    GetProfileAuthUser,
    GetUserBonusHistoryService,
    GetUserBonusService,
    GetUserCardsService,
    GetUserInfoService,
    GetUserStatusService,
    GroupAcceptInviteService,
    GroupDeclineInviteService,
    GroupDeleteService,
    GroupExitService,
    GroupMemberAddService,
    ImageUserProfileService,
    UpdateUserProfileService,
    UserGroupAddService,
    UserGroupService,
    UserPromoService};
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserController    extends ApiController
{

    /**
     * конструктор класса
     */
    public function __construct()
    {
    }

    /**
     * @OA\Get (
     *     path="/v1/user/profile",
     *     summary="Получение профиля пользователя",
     *     description="Получение профиля пользователя",
     *     security={ {"ApiKey": {}} },
     *     tags={"user"},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *              type="object",
     *              example={
     *                  "phone": "77751625741",
     *                  "email": "",
     *                  "vehicle_type": "",
     *                  "vehicle_manufacture": 0,
     *                  "company_name": null,
     *                  "city_id": 0,
     *                  "vehicle_id": null
     *               }
     *         )
     *    ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                 property="message",
     *                 type="string",
     *                 description="сообщение",
     *                 example="blocked"
     *              ),
     *              @OA\Property(
     *                 property="status",
     *                 type="int",
     *                 description="статус",
     *                 example=0
     *              ),
     *         )
     *     ),
     * )     * получение профиля пользователя
     * @return JsonResponse|object
     */
    public function profile(): JsonResponse
    {
        $response = (new GetProfileAuthUser())->make();

        return response()->json($response['data'])->setStatusCode($response['http_code']);
    }

    /**
     * получение карт пользователя
     * @return JsonResponse|object
     */
    public function cards(): JsonResponse
    {
        // получение карт пользователя
        $response = (new GetUserCardsService(Auth::id()))->make();
        return $this->result($response);
    }

    public function status(): JsonResponse
    {
        // получение карт пользователя
        $response = (new GetUserStatusService(Auth::id()))->make();
        return $this->result($response);
    }

    public function info(): JsonResponse
    {
        // получение карт пользователя
        $response = (new GetUserInfoService(Auth::user()))->make();
        return $this->result($response);
    }

    public function profileUpdate(ProfileUpdateRequest $request): JsonResponse
    {
        // получение карт пользователя
        $response = (new UpdateUserProfileService(Auth::id(), $request))->make();
        return $this->result($response);
    }

    public function bonus(): JsonResponse
    {
        // получение карт пользователя
        $response = (new GetUserBonusService(Auth::id()))->make();
        return $this->result($response);
    }

    public function bonusHistory(): JsonResponse
    {
        // получение карт пользователя
        $response = (new GetUserBonusHistoryService(Auth::id()))->make();
        return $this->result($response);
    }

    public function profileImage(ProfileImageRequest $request)
    {
        $response = (new ImageUserProfileService(Auth::id(), $request))->make();
        return $this->result($response);
    }

    public function deviceTokenUpdate(DeviceTokenRequest $request)
    {
        $response = (new DeviceTokenService(Auth::id(), $request->token_id))->make();
        return $this->result($response);
    }

    public function cumulativePromos(): JsonResponse
    {
        $response = (new UserPromoService(Auth::id()))->make();
        return $this->result($response);
    }

    public function userGroup(): JsonResponse
    {
        $response = (new UserGroupService(Auth::id()))->make();
        return $this->result($response);
    }

    public function addGroup(GroupRequest $request)
    {
        $response = (new UserGroupAddService(Auth::id(), $request->title))->make();
        return $this->result($response);
    }

    public function addMember(GroupMemberRequest $request)
    {
        $response = (new GroupMemberAddService(Auth::id(), $request->member_phone, Auth::user()->phone))->make();
        return $this->result($response);
    }

    public function acceptInvite(): JsonResponse
    {
        $response = (new GroupAcceptInviteService(Auth::id()))->make();
        return $this->result($response);
    }

    public function declineInvite(): JsonResponse
    {
        $response = (new GroupDeclineInviteService(Auth::id()))->make();
        return $this->result($response);
    }

    public function exitGroup()
    {
        $response = (new GroupExitService(Auth::id()))->make();
        return $this->result($response);
    }

    public function deleteGroup()
    {
        $response = (new GroupDeleteService(Auth::id()))->make();
        return $this->result($response);
    }



}
