<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Http\Controllers\ApiController;
use App\Http\Requests\v1\Auth\{LoginRequest};
use App\Services\v1\Auth\{GetTokenService};
use Illuminate\Http\{JsonResponse};
use Illuminate\Support\Facades\Log;

class AuthController  extends ApiController
{
    public function login(LoginRequest $request): JsonResponse
    {
        Log::info(__METHOD__ .':' .$request);
        $response = (new GetTokenService($request->usr, $request->pwd))->make();
        return $this->result($response);
    }

}
