<?php

namespace App\Http\Controllers\Api\v1\Client;

use App\Http\Controllers\ApiController;
use App\Services\v1\Client\ClientService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\v1\Client\ClientInfoRequest;

class ClientController extends ApiController
{
    /**
     * Overall information about client
     * @param Request $request
     * @return Object
     */
    public function info(ClientInfoRequest $request): JsonResponse
    {
        $response = (new ClientService($request))->info();
        return $this->result($response);
    }
}

