<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderItemDetailModel extends Model
{
    protected $table = 'crm_order_item_detail';

    public $timestamps = false;

    protected $guarded = [];
}