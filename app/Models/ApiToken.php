<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ApiToken  extends Model
{

    public $timestamps = false;

    protected $table = 'crm_api_tokens';

    protected $guarded = [];

    /**
     * Пользователь
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
