<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Group  extends Model
{
    protected $table = 'crm_groups';
    protected $guarded = [];
}