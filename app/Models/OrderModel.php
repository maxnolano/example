<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    protected $table = 'crm_orders_nipl';

    //public $timestamps = false;

    protected $guarded = [];
}