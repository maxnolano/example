<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PromoGood  extends Model
{
    protected $table = 'crm_promo_goods';
    protected $guarded = [];
}