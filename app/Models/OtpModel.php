<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OtpModel extends Model
{
    protected $table = 'crm_otp';

    public $timestamps = false;

    protected $guarded = [];
}