<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model
{
    protected $table = 'crm_notification_users';

    //public $timestamps = false;

    protected $guarded = [];
}