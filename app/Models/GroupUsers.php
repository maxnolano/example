<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GroupUsers  extends Model
{
    protected $table = 'crm_users_group';
    protected $guarded = [];
}