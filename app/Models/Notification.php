<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'crm_notification';

    //public $timestamps = false;

    protected $guarded = [];
}