<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Pump  extends Model
{
    protected $table = 'crm_pumps';
}