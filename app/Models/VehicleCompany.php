<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VehicleCompany   extends Model
{
    protected $table = 'crm_vehicle_companies';
}