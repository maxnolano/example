<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class StoreProduct  extends Model
{
    protected $table = 'crm_store_products';
}