<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SncApiKey extends Model
{
    protected $table = 'crm_snc_api_keys';
}
