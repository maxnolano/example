<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PromoGroup  extends Model
{
    protected $table = 'crm_promo_groups';
    protected $guarded = [];
}