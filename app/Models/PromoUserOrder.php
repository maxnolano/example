<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PromoUserOrder  extends Model
{
    protected $table = 'crm_promo_users_orders';
    protected $guarded = [];
}