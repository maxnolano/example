<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PromoUser  extends Model
{
    protected $table = 'crm_promo_users';
    protected $guarded = [];
}