<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PromoGroupOrder  extends Model
{
    protected $table = 'crm_promo_groups_orders';
    protected $guarded = [];
}