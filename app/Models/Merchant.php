<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    protected $table = 'crm_merchant_nipl';

    public $timestamps = false;

    protected $guarded = [];
}