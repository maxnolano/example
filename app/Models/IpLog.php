<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class IpLog extends Model
{
    public $timestamps = false;

    protected $table = 'crm_ip_log';

    protected $guarded = [];

}