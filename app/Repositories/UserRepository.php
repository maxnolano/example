<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserRepositoryInterface
{
    const ROLE_USER = 'user';
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;


    public array $fields = [
        'id', 'username', 'disabled', 'deleted', 'password', 'store_id'
    ];

    /**
     * получение пользователя по номеру телефона
     * @param string $phone
     * @param array|null $fields
     * @return User|null
     */
    public function getUserByUsername(string $username, array $fields = null): ?User
    {
        return User::select($fields == null ? $this->fields : $fields)
            ->where('username', $username)->first();
    }

    /**
     * добавление пользователя
     * @param array $data
     * @param bool $returned_object
     * @return User
     */
    public function store(array $data): User
    {
        return  User::create($data);
    }

    /**
     * проверка тестовый ли пользователь по номеру телефона
     * @param string $phone
     * @return bool
     */
    public function isTestUser(string $phone): bool
    {
        return User::where([['phone', $phone], ['test', 1]])->exists();
    }

    /**
     * получение авторизированного пользователя
     * @return User|null
     */
    public function getAuthUser(): ?User
    {
        return Auth::user();
    }

    /**
     * Отметить активацию пользователя
     * @param int $userId
     * @return bool
     */
    public function activate($userId): bool
    {
        return User::where('id', $userId)
            ->update(['user_status' => UserRepository::STATUS_ACTIVE, 'last_modified' => time()]);
    }

    public function updateProfile($userId, array $data): bool
    {
        // TODO: Implement updateProfile() method.
        return User::where('id', $userId)
            ->update($data);
    }
}
