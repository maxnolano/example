<?php


namespace App\Repositories;


use App\Models\Product;
use App\Models\Store;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class StoreRepository
{

    const STORE_TYPE_GAS_STATION = [6, 7];

    /**
     * получить список АЗС
     */
    public function gasStations(): Collection
    {
        return Store::select([
            'id', 'title', 'description', 'address', 'latitude', 'longitude',
            'store_side', 'thumbnail'
        ])
            ->where('deleted', 0)
            ->where('disabled', 0)
            ->where('latitude', '<>', 0)
            ->where('longitude', '<>', 0)
            //->whereIn('category_id', self::STORE_TYPE_GAS_STATION)
            ->get();
    }

    /**
     * получение АЗС по Id
     * @param int $id
     * @return Store
     */
    public function gasStation(int $id): ?Store
    {
        return Store::select([
            'id', 'title', 'description', 'address', 'latitude', 'longitude',
            'store_side', 'thumbnail', 'owner_id'
        ])
            ->where('deleted', 0)
            ->where('disabled', 0)
            ->where('latitude', '<>', 0)
            ->where('longitude', '<>', 0)
            //->whereIn('category_id', self::STORE_TYPE_GAS_STATION)
            ->where('id', $id)
            ->first();
    }
}
