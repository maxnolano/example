<?php


namespace App\Repositories;


use App\Models\Category;
use App\Models\Product;
use App\Models\Store;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CategoryRepository
{
    public function getGroceryCategories(int $storeId): Collection
    {
        return Category::select('*')
            ->where('deleted', 0)
            ->where('disabled', 0)
            ->where('object_type', 'grocery')
            ->where('store_id', $storeId)
            ->get();
    }

    public function getKassaCategories(int $storeId): Collection
    {
        return Category::select('*')
            ->where('deleted', 0)
            ->where('disabled', 0)
            ->where('object_type', 'kassa')
            ->where('store_id', $storeId)
            ->get();
    }

}
