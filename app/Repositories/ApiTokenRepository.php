<?php


namespace App\Repositories;


use App\Models\ApiToken;
use App\Repositories\Interfaces\ApiTokenRepositoryInterface;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;

class ApiTokenRepository implements ApiTokenRepositoryInterface
{
    // тип токена мобильный означает что токен
    // был сгенерирован для мобильного устройтсва
    public const TOKEN_TYPE_MOBILE = 'mobile_api';

    /**
     * обновление токена
     * @param int $userId
     * @return ApiToken
     */
    public function refreshToken(int $userId): ApiToken
    {
        // удаление текущего токена пользователя
        ApiToken::where('user_id', $userId)->delete();

        // новый токен
        return ApiToken::create([
            'user_id' => $userId,
            'token' => $this->createNewToken(),
            'date_added' => time(),
            'expired_date' => strtotime('+30 day'),
            'disabled' => 0,
            'deleted' => 0
        ]);
    }

    /**
     * Create a new token for the user.
     *
     * @return string
     */
    private function createNewToken(): string
    {
        $key = Config::get('app.key');

        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }
        return hash_hmac('sha256', Str::random(40), $key);
    }
}
