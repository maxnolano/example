<?php

namespace App\Repositories;

use App\Models\StoreProduct;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 * репозиторий для работы с товарами  магазинов (АЗС)
 * @package App\Repositories
 */
class ProductRepository implements ProductRepositoryInterface
{


    const PRODUCT_CATEGORY_FUEL = 66;
    /**
     * получение списка товаров для конкретной азс
     * @param int $storeId
     * @param int $userId
     * @return Collection|null
     */
    public function getProducts(int $storeId, int $userId): ?Collection
    {
        return Pump::select([
            'crm_pumps.pump_id', 'crm_pumps.title as pump_title',
            'crm_pumps.columnid', 'crm_pumps.store_product_id',
            'crm_products.title as product_title', 'crm_store_products.price as product_price'
        ])
            ->orderBy('crm_pumps.title', 'ASC')
            ->where([
                ['crm_pumps.store_id', $storeId],
                ['crm_pumps.disabled', 0],
                ['crm_pumps.deleted', 0],
            ])
            ->join('crm_store_products', function ($join) {
                $join->on('crm_store_products.id', 'crm_pumps.store_product_id');
                $join->where('crm_store_products.status', 'active');
            })
            ->join('crm_products', function ($join) {
                $join->on('crm_products.id', 'crm_store_products.product_id');
            })->get();
    }

    /**
     * получение продуктов у пользователя с учетом баланса ( корпоротивного )
     * @param int $storeId
     * @param int $userId
     * @return Collection|null
     */
    public function getProductsWithAmountBalance(int $storeId, int $userId): ?Collection
    {
        return StoreProduct::select([
            'crm_store_products.id as spid', 'crm_products.id as pid',
            'crm_store_products.price as store_product_price', 'crm_products.title',
            'crm_amount_balance.amount_balance'
        ])->join('crm_products', function ($join) {
            $join->on('crm_store_products.product_id', 'crm_products.id');
        })->leftJoin('crm_amount_balance', function ($join) {
            $join->on('crm_amount_balance.gas_type', 'crm_products.parent_product_id');
        })->where([
            ['crm_store_products.store_id', $storeId], ['crm_store_products.status', 'active'],
            ['crm_store_products.deleted', 0], ['crm_store_products.disabled', 0],
            ['crm_products.disabled', 0], ['crm_products.deleted', 0],
            ['crm_amount_balance.user_id', $userId]
        ])->orderBy('crm_store_products.id', 'ASC')->get();
    }

    /**
     * получение продуктов у пользователя с учетом баланса ( литрового )
     * @param int $storeId
     * @param int $userId
     * @return Collection|null
     */
    public function getProductsWithLitreBalance(int $storeId, int $userId): ?Collection
    {
        return StoreProduct::select([
            'crm_store_products.id as spid', 'crm_products.id as pid',
            'crm_store_products.price as store_product_price', 'crm_products.title',
            'crm_liter_balance.litre_balance'
        ])->join('crm_products', function ($join) {
            $join->on('crm_store_products.product_id', 'crm_products.id');
        })->leftJoin('crm_liter_balance', function ($join) {
            $join->on('crm_liter_balance.gas_type', 'crm_products.id');
        })->where([
            ['crm_store_products.store_id', $storeId], ['crm_store_products.status', 'active'],
            ['crm_store_products.deleted', 0], ['crm_store_products.disabled', 0],
            ['crm_products.disabled', 0], ['crm_products.deleted', 0],
            ['crm_liter_balance.user_id', $userId]
        ])->orderBy('crm_store_products.id', 'ASC')->get();
    }

    /**
     * todo: удалить потом эот метод
     * получение списка топлива с ценами по АЗС
     * @param int $storeId
     * @return Collection
     */
    public function getFuelProducts(int $storeId): Collection
    {
        $expression = StoreProduct::select([
            'crm_store_products.id as spid',
            'crm_store_products.price as store_product_price',
            'crm_products.title'
        ])->join('crm_products', function ($join) {
            $join->on('crm_store_products.product_id', 'crm_products.id');
        })
            ->where('crm_store_products.store_id', $storeId)
            ->whereRaw('crm_store_products.status=\'active\' and crm_store_products.deleted=0 and crm_store_products.disabled=0')
            ->whereRaw('crm_products.disabled = 0 and crm_products.deleted=0')
            ->where('crm_products.category_id', self::PRODUCT_CATEGORY_FUEL)
            ->orderBy('crm_store_products.id', 'ASC');

        Log::info($expression->toSql());
        return $expression->get();
    }

    public function getGroceryProducts(int $storeId, array $categoryId): Collection
    {
       /*csp.id store_product_id,csp.store_id,csp.product_id,
                csp.price product_price,prod.description,prod.title product_name,
					 prod.thumbnail product_thumbnail,
                prod.category_id
                from crm_store_products csp,crm_products prod
                where csp.store_id = 1
    and csp.status = 'active'
    and prod.id = csp.product_id
    and prod.category_id IN ( SELECT cat.id FROM crm_categories cat WHERE cat.object_type='normal')
                and prod.disabled=0
    and prod.deleted=0
                ORDER BY prod.category_id
*/
        $expression = StoreProduct::select([
            'crm_store_products.id as store_product_id',
            'crm_store_products.store_id',
            //'crm_store_products.product_id',
            'crm_store_products.price as product_price',
            'crm_products.description',
            'crm_products.title as product_name',
            'crm_products.thumbnail as product_thumbnail',
            'crm_products.category_id'
        ])->join('crm_products', function ($join) {
            $join->on('crm_store_products.product_id', 'crm_products.id');
        })
            ->where('crm_store_products.store_id', $storeId)
            ->whereRaw('crm_store_products.status=\'active\' and crm_store_products.deleted=0 and crm_store_products.disabled=0')
            ->whereRaw('crm_products.disabled = 0 and crm_products.deleted=0')
            ->whereIn('crm_products.category_id', $categoryId)
            ->orderBy('crm_store_products.id', 'ASC');


        //Log::info($expression->toSql());
        return $expression->get();

    }

    public function getProductsByStoreAndOrderId(int $storeId, int $orderId): Collection
    {
        /*select cat.object_type,store.address,det.id as oid_id,store.title as store_name,
                store.img as store_img,store.thumbnail as store_thumbnail,prod.title product_name,
                prod.img as product_img,prod.thumbnail as product_thumbnail,store.id store_id,
                det.product_id product_id,det.amt,det.quantity
                from crm_products prod,crm_stores store,crm_order_item_detail det,
                crm_store_products store_prod,crm_categories cat
                where cat.id = prod.category_id
    and store_prod.store_id = :store_id
    and store_prod.id = det.product_id
    and det.order_nipl_id = :order_id
    and prod.id = store_prod.product_id
    and store.id = :store_id
*/
        $expression = StoreProduct::select([
            'cat.object_type','store.address','det.id as oid_id','store.title as store_name',
                'store.img as store_img','store.thumbnail as store_thumbnail','prod.title as product_name',
                'prod.img as product_img','prod.thumbnail as product_thumbnail','store.id as store_id',
                'det.product_id as product_id','det.amt','det.quantity'
        ])->join('crm_products as prod', 'crm_store_products.product_id', '=', 'prod.id')

            ->join('crm_order_item_detail as det', 'crm_store_products.id', '=', 'det.product_id')
            ->join('crm_stores as store', 'store.id', '=', 'crm_store_products.store_id')
            ->join('crm_categories as cat', 'cat.id', '=', 'prod.category_id')

            ->where('store.id', $storeId)
            ->where('det.order_nipl_id', $orderId);
            //->orderBy('crm_store_products.id', 'ASC');

        Log::info($expression->toSql());
        return $expression->get();
    }
}
