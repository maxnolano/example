<?php

namespace App\Repositories\Order;

use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Models\OrderModel;
use App\Models\OrderItemDetailModel;
use Illuminate\Pagination\Paginator;

class OrderRepository implements OrderRepositoryInterface
{

    public function __construct()
    {}

    /**
     * Create order
     * @param Array $request
     * @return Object
     */
    public function createOrder(Array $request): array
    {
        $created_status = OrderModel::create([
            'order_for' => $request['order_for'],
            'is_grocery' => 1,
            'service_type' => 2,
            'kassa_status' => 1,
            'total_order_amt' => $request['total_order_amt'],
            'cashier_id' => $request['cashier_id'],
            'store_id' => $request['store_id'],
            'user_id' => $request['user_id'],
            'product_id' => 0,
            'demand_quantity' => 0,
            'title' => '',
            'payment_from' => '',
            'pump_number_title' => '',
            'inserted_date' => time()
        ]);

        $order_details = [];

        foreach($request['products'] as $product){
            $order_details[] = OrderItemDetailModel::create([
                'order_nipl_id' => $created_status->id,
                'product_id' => $product['store_product_id'],
                'amt' => $product['product_price'],
                'quantity' => $product['amount'],
            ]);
        }

        $created_status->order_item_details = $order_details;

        return ['created_status' => $created_status];
    }

    /**
     * List of orders
     * @param Array $request
     * @return Object
     */
    public function orders(Array $request): object
    {
        return DB::table('crm_orders_nipl as npl')
            ->join('crm_order_item_detail as dt', 'npl.id', '=', 'dt.order_nipl_id')
            ->join('crm_users as u', 'npl.user_id', '=', 'u.id')
            ->join('crm_products as p', 'dt.product_id', '=', 'p.id')
            ->join('crm_categories as c', 'p.category_id', '=', 'c.id')
            ->where('npl.service_type', '=', '0')
            ->where('npl.kassa_status', '=', $request['kassa_status'])
            ->where('npl.store_id', '=', $request['store_id'])
            ->select(
                'npl.id as order_id', 'u.display_name as user_name', 'dt.amt as total_price', 'dt.quantity as total_quantity', 'p.title as product_title', 'c.title as category_title'
            )
            ->orderBy('npl.id', 'desc')
            ->get()
            ;
    }

    /**
     * Change order status
     * @param Array $request
     * @return Object
     */
    public function change(Array $request): int
    {
        return DB::table('crm_orders_nipl')
            ->where('id', $request['order_id'])
            ->update(['kassa_status' => $request['kassa_status']]);
    }

}