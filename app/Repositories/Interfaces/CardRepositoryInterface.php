<?php


namespace App\Repositories\Interfaces;


use Illuminate\Support\Collection;

interface CardRepositoryInterface
{
    /**
     * получение списка карта по айди юзера
     * @param int $id
     * @return mixed
     */
    public function getCardByUserId(int $id) :Collection;
}