<?php


namespace App\Repositories\Interfaces;


use App\Models\UserBonusTotal;
use Illuminate\Support\Collection;

interface UserBonusTotalRepositoryInterface
{
    public function getBonusTotalByUserId(int $userId) :?UserBonusTotal;

    public function updateBonusTotal(int $userId, $amount): bool;

}