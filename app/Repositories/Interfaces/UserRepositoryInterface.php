<?php


namespace App\Repositories\Interfaces;


use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;

interface UserRepositoryInterface
{
    /**
     * получение активного в системе пользователя по номеру телефона
     * @param string $phone
     * @param array|null $fields
     * @return User|null
     */
    public function getUserByUsername(string $username, array $fields = null): ?User;

    /**
     * добавление пользователя
     * @param array $data
     * @return User
     */
    public function store(array $data): User;

    /**
     * проверка тестовый ли пользователь по номеру телефона
     * @param string $phone
     * @return bool
     */
    public function isTestUser(string $phone): bool;

    /**
     * получение авторизированного пользователя
     * @return User|null
     */
    public function getAuthUser(): ?User;

    /**
     * Отметить активацию пользователя
     * @param int $userId
     * @return bool
     */
    public function activate($userId): bool;

    public function updateProfile($userId, array $data): bool;

}
