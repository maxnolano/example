<?php

namespace App\Repositories\Interfaces;

interface IpLogRepositoryInterface
{

    /**
     * Отметить в журнале
     * @param string $phone
     * @return void
     */
    public function register($ip);
}
