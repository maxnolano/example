<?php


namespace App\Repositories\Interfaces;


use App\Models\ApiToken;

interface ApiTokenRepositoryInterface
{

    /**
     * обновление токена
     * @param int $userId
     * @param array $data
     * @return ApiToken
     */
    public function refreshToken(int $userId): ApiToken;
}
