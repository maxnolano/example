<?php


namespace App\Repositories\Interfaces;


use App\Models\UserBonusHistory;
use Illuminate\Support\Collection;

interface UserBonusHistoryRepositoryInterface
{
    public function add(array $data): bool;

}