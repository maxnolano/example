<?php


namespace App\Repositories\Interfaces;


use Illuminate\Support\Collection;

interface ProductRepositoryInterface
{
    /**
     * получение списка товаров для конкретной азс
     * @param int $storeId
     * @param int $userId
     * @return Collection|null
     */
    public function getProducts(int $storeId,int $userId) : ?Collection;

    /**
     * получение продуктов у пользователя с учетом баланса
     * @param int $storeId
     * @param int $userId
     * @return Collection|null
     */
    public function getProductsWithAmountBalance(int $storeId,int $userId) : ?Collection;

    /**
     * получение продуктов у пользователя с учетом баланса
     * @param int $storeId
     * @param int $userId
     * @return Collection|null
     */
    public function getProductsWithLitreBalance(int $storeId,int $userId) : ?Collection;

}