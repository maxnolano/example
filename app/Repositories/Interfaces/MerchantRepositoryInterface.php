<?php


namespace App\Repositories\Interfaces;

use App\Models\Merchant;

interface MerchantRepositoryInterface
{
    public function get(int $id): ?Merchant;
    public function getByStoreId(int $storeId): ?Merchant;
}
