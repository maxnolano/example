<?php


namespace App\Repositories\Interfaces;


use Illuminate\Support\Collection;

interface BeelineRepositoryInterface
{
    /**
     * сохранение номера пользователя для оплаты через beeline
     * @param int $userId
     * @param string $phone
     * @return bool
     */
    public function storePhone(int $userId,string $phone) : bool;

    /**
     * проверка на существование номера
     * @param string $phone
     * @return bool
     */
    public function existsPhone(string $phone) : bool;

    /**
     * получение всех номеров по айди пользователя
     * @param int $userId
     * @return array
     */
    public function getPhonesByUserId(int $userId) : array;

    /**
     * установка номера по умолчанию
     * @param int $userId
     * @param $phoneId
     * @return mixed
     */
    public function setDefaultPhone(int $userId,$phoneId) : bool;

    /**
     * удаление телефона ( псевдоудаление )
     * @param int $userId
     * @param $phoneId
     * @return bool
     */
    public function destroyPhone(int $userId,$phoneId) : bool;

}