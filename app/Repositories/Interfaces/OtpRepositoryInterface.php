<?php


namespace App\Repositories\Interfaces;


use App\Models\OtpModel;

interface OtpRepositoryInterface
{
    /**
     * Запись нового OTP
     * @param string $phone
     * @param array $otp
     * @return void
     */
    public function newOtp(string $phone, int $otp);

    /**
     * возвращает экзепляр класса OtpModel или null
     * @param string $phone
     * @param int $code
     * @return OtpModel|null
     */
    public function get(string $phone, int $code): ?OtpModel;
}
