<?php


namespace App\Repositories\Interfaces;

use App\Models\Promo;
use Illuminate\Database\Eloquent\Collection;

interface PromoRepositoryInterface
{

    public function listAll(): ?Collection;
    public function get(int $id): ?Promo;
    //public function getByStoreId(int $storeId): ?Merchant;
}
