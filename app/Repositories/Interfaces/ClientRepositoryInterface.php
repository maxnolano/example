<?php

namespace App\Repositories\Interfaces;

use App\Models\OrderModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface ClientRepositoryInterface
{
    /**
     * Overall information about client
     * @param Array $request
     * @return Object
     */
    public function info(Array $request): object;
}