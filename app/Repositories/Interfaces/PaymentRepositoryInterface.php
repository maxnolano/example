<?php


namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;

/**
 * интерфейс для работы с оплатой
 * @package App\Repositories\Interfaces
 */
interface PaymentRepositoryInterface
{
    /**
     * получение кард пользователя
     * @param int $userId
     * @param array $fields
     * @return Collection|null
     */
    public function getCards(int $userId,array $fields) : ?Collection;

    /**
     * получение сессий по айди юзера
     * @param int $userId
     * @return string|null
     */
    public function getSession(int $userId) : ?string;

}