<?php


namespace App\Repositories\Interfaces;


use Illuminate\Support\Collection;

interface VehicleCompanyRepositoryInterface
{
    /**
     * получение всех видов транспорта
     * @return Collection|null
     */
    public function getAllVehicleList() : ?Collection;

    /**
     * получение списка транспорта по айди компаний
     * @param int $companyId
     * @return Collection|null
     */
    public function getVehicleModelByCompany(int $companyId) : ?Collection;

}