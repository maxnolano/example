<?php

namespace App\Repositories\Interfaces;

use App\Models\OrderModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface OrderRepositoryInterface
{
    /**
     * Create order
     * @param Array $request
     * @return Object
     */
    public function createOrder(Array $request): array;

    /**
     * List of orders
     * @param Array $request
     * @return Object
     */
    public function orders(Array $request): object;

    /**
     * Change order status
     * @param Array $request
     * @return Object
     */
    public function change(Array $request): int;

    // /**
    //  * Get order
    //  * @param int $orderId
    //  * @return Collection
    //  */
    // public function get(int $orderId):?Collection;
}