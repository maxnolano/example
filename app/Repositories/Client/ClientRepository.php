<?php

namespace App\Repositories\Client;

use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\ClientRepositoryInterface;
use App\Models\OrderModel;
use App\Models\OrderItemDetailModel;
use Illuminate\Pagination\Paginator;

class ClientRepository implements ClientRepositoryInterface
{

    public function __construct()
    {}

    /**
     * Overall client information
     * @param Array $request
     * @return Object
     */
    public function info(Array $request): object
    {
        return DB::table('crm_orders_nipl as npl')
            ->join('crm_order_item_detail as dt', 'npl.id', '=', 'dt.order_nipl_id')
            ->join('crm_users as u', 'npl.user_id', '=', 'u.id')
            ->join('crm_products as p', 'dt.product_id', '=', 'p.id')
            ->join('crm_categories as c', 'p.category_id', '=', 'c.id')
            ->join('crm_vehicle_companies as vc', 'u.vehicle_company_id', '=', 'vc.id')
            ->join('crm_vehicle_models as vm', 'u.vehicle_model_id', '=', 'vm.id')
            ->where('u.uuid', '=', $request['uuid'])
            ->where('npl.service_type', '=', '0')
            ->select(
                'npl.id as order_id', 'npl.kassa_status as order_status', 'u.display_name as user_name', 'u.phone as phone', 'u.vehicle_plate_number as plate_number', 'vc.title as vehicle_brand', 'vm.title as vehicle_model',  'dt.amt as total_price', 'dt.quantity as total_quantity', 'p.title as product_title', 'c.title as category_title'
            )
            ->orderBy('npl.id', 'desc')
            ->get()
            ;
    }
}