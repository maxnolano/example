<?php

namespace App\Services\v1\Client;

use App\Services\v1\BaseService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Interfaces\ClientRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Support\Collection;

/**
 * Service for creating orders
 */
class ClientService extends BaseService
{
    // Incoming request
    private $request;

    // Repository for creating an order
    private ClientRepositoryInterface $clientRepository;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // OrderRepository initialization
        $this->clientRepository = new ClientRepository();
    }

    /**
     * Overall information about client
     * @return Object
     */
    public function info(): array
    {
        try{
            $request_params = [];
            $request_params['uuid'] = $this->request['uuid'];

            $data = $this->clientRepository->info($request_params);

            $overall_data = $data->toArray();

            $o_id = $overall_data[0]->order_id;

            $orders = [];

            $order_items = [];

            $numItems = count($overall_data);

            $i = 0;

            foreach($overall_data as $o){
                if($o->order_id != $o_id){
                    array_push($orders, (object)[
                        'order_id' => $o_id,
                        'order_items' => $order_items,
                    ]);
                    $order_items = [];
                    $o_id = $o->order_id;
                }

                array_push($order_items, (object)[
                    'user_name' => $o->user_name,
                    'total_price' => $o->total_price,
                    'total_quantity' => $o->total_quantity,
                    'product_title' => $o->product_title,
                    'category_title' => $o->category_title,
                    'order_status' => $o->order_status
                ]);

                if(++$i === $numItems) {
                    array_push($orders, (object)[
                        'order_id' => $o_id,
                        'order_items' => $order_items,
                    ]);
                }
            }

            $overall_result = [];

            $overall_result['user_name'] = $overall_data[0]->user_name;
            $overall_result['phone'] = $overall_data[0]->phone;
            $overall_result['plate_number'] = $overall_data[0]->plate_number;
            $overall_result['vehicle_brand'] = $overall_data[0]->vehicle_brand;
            $overall_result['vehicle_model'] = $overall_data[0]->vehicle_model;
            $overall_result['orders'] = $orders;
            $overall_result['gifts'] = [];
            $overall_result['promos'] = [];
            $overall_result['progress'] = [];

            return $this->result(['data' => $overall_result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
