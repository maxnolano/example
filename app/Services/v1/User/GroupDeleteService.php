<?php

namespace App\Services\v1\User;

use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Presenters\v1\PromoGoodPresenter;
use App\Presenters\v1\PromoPresenter;
use App\Repositories\{CardMpRepository,
    CumulativePromoRepository,
    GroupsPromoRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\PromoRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    PromoGroupOrderRepository,
    PromoUserOrderRepository,
    UserGroupRepository,
    UserRepository};
use App\Services\v1\BaseService;

class GroupDeleteService extends BaseService
{
    //private $request;
    private int $userId;

    private UserGroupRepository $groupRepository;
    private $promoUserOrderRepository;
    private $promoGroupOrderRepository;
    private $promoGroupRepository;
    private $cumulativePromoRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->groupRepository = new UserGroupRepository();
        $this->promoUserOrderRepository = new PromoUserOrderRepository();
        $this->promoGroupOrderRepository = new PromoGroupOrderRepository();
        $this->promoGroupRepository = new GroupsPromoRepository();
        $this->cumulativePromoRepository = new CumulativePromoRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {

        $groupMember = $this->groupRepository->getMemberGroup($this->userId);
        //print_r($groupMember);

        if (empty($groupMember)) {
            return $this->error(400, 'Вы не в группе');
        }

        if ($groupMember->is_owner != 1) {
            return $this->error(400, 'Невозможно удалить группу. Вы не владелец группы.');
        }

        //$group = $this->groupRepository->addGroup($this->userId, $this->title);

        $promoGroups = $this->promoGroupRepository->listByGroupId($groupMember->group_id);
        foreach($promoGroups->toArray() as $group) {
            $this->cumulativePromoRepository->addToUser($group['promo_id'], $this->userId);
        }

        $this->groupRepository->deleteGroup($this->userId);
        $this->promoGroupRepository->deleteGroup($groupMember->group_id);

        $groupOrders = $this->promoGroupOrderRepository->getAllOrdersByGroup($groupMember->group_id);
        if (!empty($groupOrders)) {
            $groupOrders = $groupOrders->toArray();
            foreach ($groupOrders as $order) {
                $this->promoUserOrderRepository->addOrderToUser($this->userId, $order['promo_id'],
                    $order['order_id'], $order['promo_good_id'],$order['amount'], $order['amount_unit']);
            }
            $this->promoGroupOrderRepository->setDeletedGroupOrder($groupMember->group_id);
        }

        //print_r($goods);
        $data['message'] = 'Группа удалена';
        //$data['promo_goods'] = $promo_goods;
        return $this->result($data);

    }
}
