<?php

namespace App\Services\v1\User;

use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Presenters\v1\GroupMemberPresenter;
use App\Presenters\v1\PromoGoodPresenter;
use App\Presenters\v1\PromoPresenter;
use App\Repositories\{CardMpRepository,
    CumulativePromoRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\PromoRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    UserGroupRepository,
    UserRepository};
use App\Services\v1\BaseService;

class UserGroupService extends BaseService
{
    //private $request;
    private int $userId;

    // репозиторий для работы с картами
    private $groupRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->groupRepository = new UserGroupRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        //$userPromo = $this->promoRepository->getUserAllPromo($this->userId)->toArray();
        $group_users = [];
        $group = $this->groupRepository->getCurrent($this->userId);
        if (!empty($group))
            $group_users = $this->groupRepository->getGroupMembers($group->id)->present(GroupMemberPresenter::class)
                ->map(function ($model) {
                    return $model->getItem();
                });
            //$group_users = $this->groupRepository->getGroupMembers($group->id);

        //print_r($goods);
        $data['group'] = $group;
        $data['group']['users'] = $group_users;
        return $this->result($data);

    }
}
