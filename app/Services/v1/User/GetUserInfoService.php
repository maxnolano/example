<?php

namespace App\Services\v1\User;

use App\Presenters\v1\CardPresenter;
use App\Repositories\{CardMpRepository, Interfaces\CardRepositoryInterface};
use App\Services\v1\BaseService;

class GetUserInfoService extends BaseService
{
    // айди пользователя
    private $user;

    // репозиторий для работы с картами
    private CardRepositoryInterface $cardRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->cardRepository = new CardMpRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        $cards = $this->cardRepository->getCardByUserId($this->user->id)->present(CardPresenter::class)
            ->map(function ($model) {
                return $model->item();
            });

        return $this->result(
            [
                'disabled' => $this->user->disabled,
                'deleted' => $this->user->deleted,
                'totalCard' => count($cards),
                'default_payment_selection' => 1
            ]);
    }
}
