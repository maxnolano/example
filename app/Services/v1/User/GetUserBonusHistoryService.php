<?php

namespace App\Services\v1\User;

use App\Presenters\v1\BonusHistoryPresenter;
use App\Presenters\v1\BonusPresenter;
use App\Presenters\v1\CardPresenter;
use App\Repositories\{CardMpRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\UserBonusHistoryRepositoryInterface,
    Interfaces\UserBonusTotalRepositoryInterface,
    UserBonusHistoryRepository,
    UserBonusTotalRepository};
use App\Services\v1\BaseService;

class GetUserBonusHistoryService extends BaseService
{
    // айди пользователя
    private int $userId;

    // репозиторий для работы с картами
    private UserBonusHistoryRepositoryInterface $bonusHistoryRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->bonusHistoryRepository = new UserBonusHistoryRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        $history = $this->bonusHistoryRepository->getHistoryByUser($this->userId)->present(BonusHistoryPresenter::class)
            ->map(function ($model) {
                return $model->item();
            });

        return $this->result(['history' => $history->toArray()]);
    }
}
