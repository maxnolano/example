<?php

namespace App\Services\v1\User;

use App\Presenters\v1\UserPresenter;
use App\Repositories\UserRepository;

class GetProfileAuthUser
{
    /**
     * получение профиля пользователя
     * @return array
     */
    public function make()
    {

        $userRepository = new UserRepository();

        return [
            'data' => (new UserPresenter($userRepository->getAuthUser()))->getProfile(),
            'http_code' => 200
        ];
    }
}
