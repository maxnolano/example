<?php

namespace App\Services\v1\User;

use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Presenters\v1\PromoGoodPresenter;
use App\Presenters\v1\PromoPresenter;
use App\Repositories\{CardMpRepository,
    CumulativePromoRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\PromoRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    UserGroupRepository,
    UserRepository};
use App\Services\v1\BaseService;

class UserPromoService extends BaseService
{
    //private $request;
    private int $userId;

    // репозиторий для работы с картами
    private PromoRepositoryInterface $promoRepository;
    private $groupRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->promoRepository = new CumulativePromoRepository();
        $this->groupRepository = new UserGroupRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        //$userPromo = $this->promoRepository->getUserAllPromo($this->userId)->toArray();

        $groupCurrent = $this->groupRepository->getCurrent($this->userId);

        $userPromo = $this->promoRepository->getUserAllPromo($this->userId)->present(PromoPresenter::class)
            ->map(function ($model) {
                return $model->details();
            });

        $promo_goods = [];
        $goods = $this->promoRepository->getUserPromoGoods($this->userId)->present(PromoGoodPresenter::class)
            ->map(function ($model) {
                return $model->item();
            })->toArray();
        foreach ($goods as $good) {
            $promo_goods[$good['promo_id']][] = $good;
        }

        if (!empty($userPromo)) {
            $userPromo = $userPromo->toArray();
            foreach ( $userPromo as $k=>$v) {
                $userPromo[$k]['goods'] = $promo_goods[$v['id']];
            }
        }

        //print_r($goods);
        $data['promo'] = $userPromo;
        //$data['promo_goods'] = $promo_goods;
        return $this->result($data);

    }
}
