<?php

namespace App\Services\v1\User;

use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Repositories\{CardMpRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    UserRepository};
use App\Services\v1\BaseService;

class ImageUserProfileService extends BaseService
{
    private $request;
    private int $userId;

    // репозиторий для работы с картами
    private UserRepositoryInterface $userRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId, $request)
    {
        $this->userId = $userId;
        $this->request = $request;
        $this->userRepository = new UserRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        $args = [];
        $file = $this->request->file('file_name');
        if ($file) {
            $image_info = ImageHelper::resize_images($file);
            if ($image_info) {
                $args['img'] = $image_info['img'];
                $args['thumbnail'] = $image_info['thumbnail'];
            }
        }

        $save_data = [
            'img' => $args['img'],
            'thumbnail' => $args['thumbnail'],
        ];

        if (!$this->userRepository->updateProfile($this->userId, $save_data)) {
            return $this->errService('Произошла ошибка при сохранении');
        }

        $thumbnail = ImageHelper::get_thumbnail($args['thumbnail'], 'full');

        return $this->result(
            [
                'message' => 'Изображение сохранено',
                'thumbnail' => $thumbnail
            ]);
    }
}
