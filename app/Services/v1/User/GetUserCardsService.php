<?php

namespace App\Services\v1\User;

use App\Presenters\v1\CardPresenter;
use App\Repositories\{CardMpRepository, Interfaces\CardRepositoryInterface};
use App\Services\v1\BaseService;

class GetUserCardsService extends BaseService
{
    // айди пользователя
    private int $userId;

    // репозиторий для работы с картами
    private CardRepositoryInterface $cardRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->cardRepository = new CardMpRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        $cards = $this->cardRepository->getCardByUserId($this->userId)->present(CardPresenter::class)
            ->map(function ($model) {
                return $model->item();
            });

        return $this->result(['cards' => $cards->toArray()]);
    }
}
