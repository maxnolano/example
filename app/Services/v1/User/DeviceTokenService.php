<?php

namespace App\Services\v1\User;

use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Repositories\{CardMpRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    UserRepository};
use App\Services\v1\BaseService;

class DeviceTokenService extends BaseService
{
    private $deviceToken;
    private int $userId;

    private UserRepositoryInterface $userRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId, $deviceToken)
    {
        $this->userId = $userId;
        $this->deviceToken = $deviceToken;
        $this->userRepository = new UserRepository();
    }

    public function make(): array
    {
        $save_data = [
            'fb_device_token' => $this->deviceToken
        ];

        if (!$this->userRepository->updateProfile($this->userId, $save_data)) {
            return $this->errService('Произошла ошибка при сохранении');
        }

        return $this->result(
            [
                'message' => 'Token сохранен'
            ]);
    }
}
