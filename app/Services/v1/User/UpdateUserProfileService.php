<?php

namespace App\Services\v1\User;

use App\Presenters\v1\CardPresenter;
use App\Repositories\{CardMpRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    UserRepository};
use App\Services\v1\BaseService;

class UpdateUserProfileService extends BaseService
{
    private $request;
    private int $userId;

    // репозиторий для работы с картами
    private UserRepositoryInterface $userRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId, $request)
    {
        $this->userId = $userId;
        $this->request = $request;
        $this->userRepository = new UserRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        $data = $this->request->toArray();

        /*$save_data = [
            'display_name' => $data['display_name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            //'phone' => $data['phone'],
            'vehicle_model_id' => $data['vehicle_model_id'],
            'vehicle_company_id' =>$data['vehicle_company_id'],
            'vehicle_plate_number' => $data['vehicle_plate_number'],
            'city_id' => $data['city_id'],
            'iin' => $data['iin']
        ];*/

        if (!$this->userRepository->updateProfile($this->userId, $data)) {
            return $this->errService('Произошла ошибка при сохранении');
        }

        return $this->result(
            [
                'message' => 'Профиль сохранен'
            ]);
    }
}
