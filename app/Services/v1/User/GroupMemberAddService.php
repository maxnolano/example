<?php

namespace App\Services\v1\User;

use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Presenters\v1\PromoGoodPresenter;
use App\Presenters\v1\PromoPresenter;
use App\Repositories\{CardMpRepository,
    CumulativePromoRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\PromoRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    UserGroupRepository,
    UserRepository};
use App\Services\v1\BaseService;

class GroupMemberAddService extends BaseService
{
    //private $request;
    private int $userId;
    private string $memberPhone;
    private string $currentUserPhone;

    private UserGroupRepository $groupRepository;
    private $userRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId, $memberPhone, $currentUserPhone)
    {
        $this->userId = $userId;
        $this->memberPhone = $memberPhone;
        $this->currentUserPhone =  $currentUserPhone;
        $this->groupRepository = new UserGroupRepository();
        $this->userRepository = new UserRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        $currentGroup = $this->groupRepository->getCurrent($this->userId);

        if (empty($currentGroup)) {
            return $this->error(400, 'У вас еще нет группы');
        }

        $currentGroupMembers = $this->groupRepository->getGroupMembers($currentGroup->id);

        if (count($currentGroupMembers->toArray()) > 2) {
            return $this->error(400, 'Превышен лимит пользователей группы');
        }

        $member = $this->userRepository->getUserByPhone($this->memberPhone, ['id', 'phone']);

        if (empty($member)) {
            return $this->error(400, 'Такого пользователя не существует');
        }

        $groupMember = $this->groupRepository->getMemberGroup($member->id);
        //print_r($groupMember);

        if (!empty($groupMember)) {
            return $this->error(400, 'Пользователь уже состоит в текущей или другой группе');
        }

        if ($member->phone == $this->currentUserPhone) {
            return $this->error(400, 'Невозможно отправить приглашение');
        }

        //$group = $this->groupRepository->addGroup($this->userId, $this->title);
        $this->groupRepository->addMemberToGroup($currentGroup->id, $member->id, 0);

        //print_r($goods);
        $data['message'] = 'Приглашение отправлено';
        //$data['promo_goods'] = $promo_goods;
        return $this->result($data);

    }
}
