<?php

namespace App\Services\v1\User;

use App\Presenters\v1\BonusPresenter;
use App\Presenters\v1\CardPresenter;
use App\Repositories\{CardMpRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\UserBonusTotalRepositoryInterface,
    UserBonusTotalRepository};
use App\Services\v1\BaseService;

class GetUserBonusService extends BaseService
{
    // айди пользователя
    private int $userId;

    // репозиторий для работы с картами
    private UserBonusTotalRepositoryInterface $bonusRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->bonusRepository = new UserBonusTotalRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        $bonusData = $this->bonusRepository->getBonusTotalByUserId($this->userId);

        return $this->result([
            'bonus_amount' => !empty($bonusData) ? $bonusData['total_amount'] : 0
             ]
        );
    }
}
