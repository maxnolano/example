<?php

namespace App\Services\v1\User;

use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Presenters\v1\PromoGoodPresenter;
use App\Presenters\v1\PromoPresenter;
use App\Repositories\{CardMpRepository,
    CumulativePromoRepository,
    GroupsPromoRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\PromoRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    PromoGroupOrderRepository,
    PromoUserOrderRepository,
    UserGroupRepository,
    UserRepository};
use App\Services\v1\BaseService;

class UserGroupAddService extends BaseService
{
    //private $request;
    private int $userId;
    private string $title;

    // репозиторий для работы с картами
    private UserGroupRepository $groupRepository;
    private $promoRepository;
    private $promoGroupRepository;
    private $promoUserOrderRepository;
    private $promoGroupOrderRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId, $title)
    {
        $this->userId = $userId;
        $this->title = $title;
        $this->groupRepository = new UserGroupRepository();
        $this->promoRepository = new CumulativePromoRepository();
        $this->promoGroupRepository = new GroupsPromoRepository();
        $this->promoUserOrderRepository = new PromoUserOrderRepository();
        $this->promoGroupOrderRepository = new PromoGroupOrderRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {
        $currentGroup = $this->groupRepository->getCurrent($this->userId);

        if (!empty($currentGroup)) {
            return $this->error(400, 'У вас уже есть группа');
        }

        $group = $this->groupRepository->addGroup($this->userId, $this->title);
        $this->groupRepository->addMemberToGroup($group->id, $this->userId, 1, 1);
        $userPromo = $this->promoRepository->getUserAllPromo($this->userId);
        if (!empty($userPromo)) {
            $userPromo = $userPromo->toArray();

            foreach ($userPromo as $promo) {
                //print_r($goods);
                $this->promoGroupRepository->addToGroup($promo['id'], $group->id);
            }

            $this->promoRepository->setDeletedUserPromo($this->userId);

            $userOrders = $this->promoUserOrderRepository->getAllOrdersByUser($this->userId);
            if (!empty($userOrders)) {
                $userOrders = $userOrders->toArray();
                foreach ($userOrders as $order) {
                    $this->promoGroupOrderRepository->addOrderToGroup($group->id, $order['promo_id'],
                        $order['order_id'], $order['promo_good_id'],$order['amount'], $order['amount_unit']);
                }
                $this->promoUserOrderRepository->setDeletedUserOrder($this->userId);
            }

            $data['message'] = 'группа создана';
        }
        return $this->result($data);
    }
}
