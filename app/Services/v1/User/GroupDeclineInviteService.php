<?php

namespace App\Services\v1\User;

use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Presenters\v1\PromoGoodPresenter;
use App\Presenters\v1\PromoPresenter;
use App\Repositories\{CardMpRepository,
    CumulativePromoRepository,
    Interfaces\CardRepositoryInterface,
    Interfaces\PromoRepositoryInterface,
    Interfaces\UserRepositoryInterface,
    PromoGroupOrderRepository,
    PromoUserOrderRepository,
    UserGroupRepository,
    UserRepository};
use App\Services\v1\BaseService;

class GroupDeclineInviteService extends BaseService
{
    //private $request;
    private int $userId;

    private UserGroupRepository $groupRepository;
    private $promoUserOrderRepository;
    private $promoGroupOrderRepository;

    /**
     * конструктор класса
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->groupRepository = new UserGroupRepository();
        $this->promoUserOrderRepository = new PromoUserOrderRepository();
        $this->promoGroupOrderRepository = new PromoGroupOrderRepository();
    }

    /**
     * получение списка карт
     * @return array
     */
    public function make(): array
    {

        $groupMember = $this->groupRepository->getMemberGroup($this->userId);
        //print_r($groupMember);

        if (empty($groupMember)) {
            return $this->error(400, 'Приглашение не найдено');
        }

        if ($groupMember->invite_status == 1) {
            return $this->error(400, 'Приглашение уже принято');
        }

        //$group = $this->groupRepository->addGroup($this->userId, $this->title);
        $this->groupRepository->declineInvite($this->userId);

        //print_r($goods);
        $data['message'] = 'Приглашение отклонено';
        //$data['promo_goods'] = $promo_goods;
        return $this->result($data);

    }
}
