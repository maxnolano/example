<?php

namespace App\Services\v1\Order;

use App\Services\v1\BaseService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Support\Collection;

/**
 * Service for creating orders
 */
class OrderService extends BaseService
{
    // Incoming request
    private $request;

    // Repository for creating an order
    private OrderRepositoryInterface $orderRepository;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // OrderRepository initialization
        $this->orderRepository = new OrderRepository();
    }

    /**
     * Create order
     * @return Object
     */
    public function createOrder(): array
    {
        try{
            $request_params = [];
            $request_params['cashier_id'] = $this->request['cashier_id'];
            $request_params['user_id'] = Auth::user()->id;
            $request_params['store_id'] = Auth::user()->store_id;
            $request_params['products'] = $this->request['products'];
            $request_params['order_for'] = $this->request['order_for'];
            $request_params['total_order_amt'] = $this->request['total_order_amt'];

            $result = $this->orderRepository->createOrder($request_params);

            return $this->result(['data' => $result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * List of orders
     * @return Object
     */
    public function orders(): array
    {
        try{
            $request_params = [];
            $request_params['kassa_status'] = $this->request['kassa_status'];
            $request_params['store_id'] = Auth::user()->store_id;;

            $data = $this->orderRepository->orders($request_params);

            $orders = $data->toArray();

            $o_id = $orders[0]->order_id;

            $items = [];

            $order_items = [];

            $numItems = count($orders);

            $i = 0;

            foreach($orders as $o){
                if($o->order_id != $o_id){
                    array_push($items, (object)[
                        'order_id' => $o_id,
                        'order_items' => $order_items,
                    ]);
                    $order_items = [];
                    $o_id = $o->order_id;
                }

                array_push($order_items, (object)[
                    'user_name' => $o->user_name,
                    'total_price' => $o->total_price,
                    'total_quantity' => $o->total_quantity,
                    'product_title' => $o->product_title,
                    'category_title' => $o->category_title,
                ]);

                if(++$i === $numItems) {
                    array_push($items, (object)[
                        'order_id' => $o_id,
                        'order_items' => $order_items,
                    ]);
                }
            }

            $collected_items = collect($items);

            $per_page = 10;

            $results = (new Collection($collected_items))->paginate($per_page);

            return $this->result(['data' => $results]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Change status to new order
     * @return Object
     */
    public function new(): array
    {
        try{
            $request_params = [];
            $request_params['order_id'] = $this->request['order_id'];
            $request_params['kassa_status'] = $this->request['kassa_status'];

            $result = $this->orderRepository->change($request_params);

            return $this->result(['data' => $result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Change status to in_progress order
     * @return Object
     */
    public function inProgress(): array
    {
        try{
            $request_params = [];
            $request_params['order_id'] = $this->request['order_id'];
            $request_params['kassa_status'] = $this->request['kassa_status'];

            $result = $this->orderRepository->change($request_params);

            return $this->result(['data' => $result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Change status to completed order
     * @return Object
     */
    public function completed(): array
    {
        try{
            $request_params = [];
            $request_params['order_id'] = $this->request['order_id'];
            $request_params['kassa_status'] = $this->request['kassa_status'];

            $result = $this->orderRepository->change($request_params);

            return $this->result(['data' => $result]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
