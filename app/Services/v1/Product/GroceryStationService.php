<?php

namespace App\Services\v1\Product;

use App\Presenters\v1\StationPresenter;
use App\Repositories\StoreRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Services\v1\BaseService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use App\Support\Collection;

class GroceryStationService extends BaseService
{
    // ID АЗС
    private int $id;

    private StoreRepository $storeRepository;
    private CategoryRepository $categoryRepository;
    private ProductRepository $productRepository;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
        $this->storeRepository = new StoreRepository();
        $this->categoryRepository = new CategoryRepository();
        $this->productRepository = new ProductRepository();

    }

    public function make(): array
    {
        $data = [];
        $result = [];
        $store = $this->storeRepository->gasStation($this->id)->toArray();
        $categories = $this->categoryRepository->getGroceryCategories($this->id)->toArray();
        //$data['cat'] = $category;
        $catIds = [];
        foreach ($categories as $cat) {
            $catIds[] = $cat['id'];
        }
        $products = $this->productRepository->getGroceryProducts($this->id, $catIds)->toArray();
//print_r($products);
        $productsWithCat = [];
        foreach ($products as $k => $v) {
            //$products[$k]['store_name'] = $store['title'];
            $v['store_name'] = $store['title'];
            //$v['cashier_id'] = $store['cashier_id'];
            $productsWithCat[$v['category_id']][] = $v;
            //$productsWithCat[$v['category_id']][]['store_name'] = $store['title'];
        }
        //print_r($productsWithCat);
        foreach ($categories as $k=>$v) {

            /*foreach ($products as $kk => $vv) {
                $products[$kk]['store_id'] = $store_id;
                $products[$kk]['cashier_id'] = $store['cashier_id'];
                $products[$kk]['store_name'] = $store['title'];
                $products[$kk]['wifi_name'] = (string)$store_id;
                $products[$kk]['product_thumbnail'] = HelperOperation::get_thumbnail($vv['product_thumbnail'], 'full');
            }*/

            $result[$k]['category_id'] = (int) $categories[$k]['id'];
            $result[$k]['title'] = $categories[$k]['title'];
            $result[$k]['category_thumbanil'] = '';//HelperOperation::get_thumbnail($v['thumbnail'], 'full');
            $result[$k]['store_id'] = (int) $this->id;
            $result[$k]['grocery_products'] = $productsWithCat[(int)$categories[$k]['id']];

        }

        $collected_items = collect($result);

        $per_page = 30;

        $results = (new Collection($collected_items))->paginate($per_page);

        $data['grocery_products_data'] = $results;

        return $this->result($data);
    }
}
