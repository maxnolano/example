<?php


namespace App\Services\v1\Auth;


use App\Helper\ImageHelper;
use App\Presenters\v1\CardPresenter;
use App\Repositories\{ApiTokenRepository, UserRepository};
use App\Repositories\Interfaces\{ApiTokenRepositoryInterface,
    CardRepositoryInterface,
    UserRepositoryInterface};
use App\Services\v1\BaseService;

class GetTokenService extends BaseService
{
    private string $username;

    private string $pwd;

    // репозиторий для работы с пользователями
    private UserRepositoryInterface $userRepository;

    // репозиторий для работы с токенами
    private ApiTokenRepositoryInterface $apiTokenRepository;

    // репозиторий для работы с картами
    private CardRepositoryInterface $cardRepository;

    /**
     * Конструктор класса
     * @param string $phone
     * @param int $otp
     */
    public function __construct(string $username, string $pwd)
    {
        $this->username = $username;
        $this->pwd = $pwd;
        $this->userRepository = new UserRepository();
        $this->apiTokenRepository = new ApiTokenRepository();
    }

    /**
     * получение токена
     * @return array
     * @throws \Exception
     */
    public function make(): array
    {
        // получение пользователя
        $user = $this->userRepository->getUserByUsername($this->username);

        if (empty($user)) {
            return $this->error(401, 'Пользователь не найден');
        }

        if (!password_verify( $this->pwd , $user->password )) {
            return $this->error(401, 'Пользователь не найден');
        }

        if ($user->disabled) {
            return $this->errValidate('Пользователь заблокирован');
        }



        // обновление токена
        $token = $this->apiTokenRepository->refreshToken($user->id);

        if (empty($token)) {
            throw new \Exception('Token is empty!');
        }

        return $this->result(
            [
                'access_token' => $token->token,
                'username' => $user->username,
                'store_id' => $user->store_id
            ]
        );
    }
}
