<?php


namespace App\Helper;


use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use App\Helper\SimpleImage;

class ImageHelper
{
    public static function upload_dir(){
        //return Yii::app()->params['upload_dir'];
        $base_path = $_SERVER['DOCUMENT_ROOT'];
        $upload_dir = '/var/www/html_yii/upload/';
        $upload_dir = $base_path.'/';
        return $upload_dir;
    }

    public static function get_app_data($key = null) {
        $app_data = ['thumbnails' => [
            'medium' => ['w' => 800, 'h' => 400, 'crop' => true],
            'thumbnail' => ['w' => 255, 'h' => 255, 'crop' => true],
            'banner_img' => ['w' => 570, 'h' => 328, 'crop' => true],
            'small' => ['w' => 100, 'h' => 100, 'crop' => true],
            'mini' => ['w' => 50, 'h' => 50, 'crop' => true]
        ],];
        if (!$key)
            return $app_data;

        if (isset($app_data[$key]))
            return $app_data[$key];

        return null;
    }

    public static function get_thumbnail($sizes, $size = 'thumbnail')
    {
        $UPLOAD_PATH_IMAGE = '/upload/media/';
        $UPLOAD_HOST_URL = 'http://ec2-3-65-22-228.eu-central-1.compute.amazonaws.com/upload/media/';

        if (!filter_var($sizes, FILTER_VALIDATE_URL) === false) {
            return $sizes;
        }

        $sizes = @unserialize($sizes);
        if (isset($sizes[$size]['filename']))
            //if (file_exists($_SERVER["DOCUMENT_ROOT"] . $UPLOAD_PATH_IMAGE . $sizes[$size]['folder'] . $sizes[$size]['filename']))
                return $UPLOAD_HOST_URL . $sizes[$size]['folder'] . $sizes[$size]['filename'];
        // return HOST_URL."assets/img/img_groceries_hambuger.jpg";
        return null;//HOST_URL."assets/img/img_groceries_hambuger.jpg";
    }

    public static function base32UUID()
    {
        $result = self::_custombase_convert(strrev(uniqid()), '0123456789abcdef', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        while (strlen($result) != 9) {
            $result = self::_custombase_convert(strrev(uniqid()), '0123456789abcdef', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        }
        return $result;
    }

    private static function _custombase_convert($numstring, $baseFrom = "0123456789", $baseTo = "0123456789")
    {
        $numstring = (string)$numstring;
        $baseFromLen = strlen($baseFrom);
        $baseToLen = strlen($baseTo);
        if ($baseFrom == "0123456789") { // No analyzing needed, because $numstring is already decimal
            $decVal = (int)$numstring;
        } else {
            $decVal = 0;
            for ($len = (strlen($numstring) - 1); $len >= 0; $len--) {
                $char = substr($numstring, 0, 1);
                $pos = strpos($baseFrom, $char);
                if ($pos !== FALSE) {
                    $decVal += $pos * ($len > 0 ? pow($baseFromLen, $len) : 1);
                }
                $numstring = substr($numstring, 1);
            }
        }
        if ($baseTo == "0123456789") { // No converting needed, because $numstring needs to be converted to decimal
            $numstring = (string)$decVal;
        } else {
            $numstring = FALSE;
            $nslen = 0;
            $pos = 1;
            while ($decVal > 0) {
                $valPerChar = pow($baseToLen, $pos);
                $curChar = floor($decVal / $valPerChar);

                if ($curChar >= $baseToLen) {
                    $pos++;
                } else {
                    $decVal -= ($curChar * $valPerChar);
                    if ($numstring === FALSE) {
                        $numstring = str_repeat($baseTo[1], $pos);
                        $nslen = $pos;
                    }

                    $numstring = substr($numstring, 0, ($nslen - $pos)) . $baseTo[(int)$curChar] . substr($numstring, (($nslen - $pos) + 1));
                    $pos--;
                }
            }
            if ($numstring === FALSE)
                $numstring = $baseTo[1];
        }
        return $numstring;
    }

    public static function resize_images($file, $sizes = null, $old_name = '')
    {
        //print_r($file);
        if (!$sizes)
            $sizes = self::get_app_data('thumbnails');
        $image_info = getimagesize($file->getRealPath());

        $img = self::base32UUID() . "." . self::image_types($image_info['mime']);
        $upload_dir = self::upload_dir() . "media/" . date('Y') . '/' . date('m') . '/';
        $thumbnail = serialize(self::do_resize($file->getRealPath(), $sizes, $img, $upload_dir, $old_name));
        return array('img' => $img, 'thumbnail' => $thumbnail);
    }

    public static function image_types($type)
    {
        $image_type = array('image/png' => 'png', 'image/jpeg' => 'jpg', 'image/gif' => 'gif');
        return $image_type[$type];
    }

    public static function make_folder($folderpath)
    {
        @mkdir($folderpath, 0777, true);
        @chmod($folderpath, 0777);
        // chmod parent folder
        $folder = pathinfo($folderpath);
        @chmod($folder['dirname'], 0777);
    }

    public static function do_resize($remote_url, $sizes, $filename, $upload_dir, $old_filename = '')
    {
        $data = array();
        $img = new SimpleImage();
        $img->load($remote_url);
        self::make_folder($upload_dir);
        if ($old_filename)
            @unlink($upload_dir . $old_filename);
        $filepath = $upload_dir . $filename;
        $width = $img->getWidth();
        $height = $img->getHeight();
        $img->resizeToThumb($width, $height);
        $img->save_with_default_imagetype($filepath);

        foreach ($sizes as $size_name => $size) {
            $img->load($filepath);

            if ($size['w'] == 0) {
                $new_filename = $size['h'] . 'h-' . $filename;
                if ($old_filename)
                    $new_oldfilename = $size['h'] . 'h-' . $old_filename;
            } elseif ($size['h'] == 0) {
                $new_filename = $size['w'] . 'w-' . $filename;
                if ($old_filename)
                    $new_oldfilename = $size['w'] . 'w-' . $old_filename;
            } else {
                $new_filename = $size['w'] . 'x' . $size['h'] . '-' . $filename;
                if ($old_filename)
                    $new_oldfilename = $size['w'] . 'x' . $size['h'] . '-' . $old_filename;
            }
            $folder = str_replace(self::upload_dir() . "media/", '', $upload_dir);

            $new_size = '';
            if ($size['w'] == 0) {
                if ($height > $size['h'])
                    $new_size = $img->resizeToHeight($size['h']);
            } elseif ($size['h'] == 0) {
                if ($width > $size['w'])
                    $new_size = $img->resizeToWidth($size['w']);
            } else {
                if ($height >= $size['h'] && $width >= $size['w'])
                    $new_size = $img->resizeToThumb($size['w'], $size['h']);
            }

            if ($new_size) {
                if ($old_filename)
                    @unlink($upload_dir . $new_oldfilename);
                $img->save_with_default_imagetype($upload_dir . '/' . $new_filename);
                $data[$size_name] = array(
                    'folder' => $folder,
                    'filename' => $new_filename,
                    'width' => $new_size['w'],
                    'height' => $new_size['h']
                );
            }
        }

        $data['full'] = array(
            'folder' => $folder,
            'filename' => $filename,
            'width' => $width,
            'height' => $height
        );
        return $data;
    }

}