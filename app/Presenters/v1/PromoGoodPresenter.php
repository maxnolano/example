<?php


namespace App\Presenters\v1;


use App\Helper\ImageHelper;
use App\Presenters\BasePresenter;

class PromoGoodPresenter extends BasePresenter
{
    public function item() : array
    {
        // $thumbnail = ImageHelper::get_thumbnail($this->thumbnail, 'full');
        $unit = [
            0 => '',
            1 => 'шт',
            2 => 'тенге',
            3 => 'л'
        ];

        return [
            'id' => $this->id,
            'promo_id' => $this->promo_id,
            'amount_ordered' => $this->amount_ordered ?? 0,
            'amount_to_order' => $this->amount_to_order,
            //'title' => $this->title,
            'description' => $this->description,
            //'expire_date' => $this->expire_date,
            //'thumbnail' =>  $thumbnail ?? null,
            //'store_id' => $this->store_id
            'amount_unit' => $unit[$this->amount_unit]
        ];
    }
}