<?php


namespace App\Presenters\v1;

use App\Presenters\BasePresenter;

class PumpPresenter extends BasePresenter
{
    /**
     * колонка АЗС в списке
     * @return array
     */
    public function asItem(): array
    {
        return [
            'pump_id' => $this->pump_id ?? null,
            'pump_title' => $this->pump_title ?? null,
            'store_id' => $this->store_id ?? null,
            'column_id' => $this->columnid ?? null,
            'store_product_id' => $this->store_product_id ?? null,
            'product_title' => $this->product_title ?? null,
            'product_price' => $this->product_price ?? null,
        ];
    }
}
