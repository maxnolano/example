<?php

namespace App\Presenters\v1;

use App\Presenters\BasePresenter;
use App\Repositories\PumpRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Helper\StationApi\StationApi;

class StationPresenter  extends BasePresenter
{
    /**
     * получение экземпляра АЗС при выводе в списке
     * @return array
     */
    public function item(): array
    {
        return [
            'store_id' => $this->id ?? null,
            'title' => $this->title ?? null,
            'description' => $this->description ?? null,
            'address' => $this->address ?? null,
            'latitude' => $this->latitude ?? null,
            'longitude' => $this->longitude ?? null,
            'thumbnail' => $this->getImage(),
        ];
    }

    /**
     * Информация о АЗС
     * @return array
     */
    public function info(): array
    {
        return [
            'store_id' => $this->id ?? null,
            'description' => $this->description ?? null,
            'title' => $this->title ?? null,
            'address' => $this->address ?? null,
            'thumbnail' => $this->getImage('full'),
            'latitude' => $this->latitude ?? null,
            'longitude' => $this->longitude ?? null,
            'pump_details' => $this->getPumps(),
            //'price_currency' => null, // Тута должна быть валюта, но ее не откуда взять
        ];
    }

    /**
     * урл картинки нужного размера
     * @param string $size
     * @return string|null
     */
    public function getImage($size = "thumbnail"): ?string
    {
        $result = null;
        try {
            $images = unserialize($this->thumbnail, ["allowed_classes" => false]);

            if ($images && isset($images[$size])) {
                $result = Config::get('smartgas.upload_host_url') . $images[$size]['folder'] . $images[$size]['filename'];
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        return $result;
    }

    /**
     * Инфо по колонкам АЗС
     * @return array|null
     */
    public function getPumps(): ?array
    {
        $pumpRepository = new PumpRepository();

        $pumps = $pumpRepository->getPumps($this->id);

        $pumps = (new StationApi($this->toArray()))->addPumpsInfo($pumps);

        return $pumps->present(PumpPresenter::class)
            ->map(function ($presenter) {
                return $presenter->asItem();
            })->toArray();
    }
}
