<?php

namespace App\Presenters\v1;

use App\Presenters\BasePresenter;
use App\Repositories\PumpRepository;
use Illuminate\Support\Facades\Config;

class BonusHistoryPresenter  extends BasePresenter
{
    public function item(): array
    {
        return [
            'order_id' => $this->order_id ?? null,
            'amount' => (int)$this->amount ?? 0,
            'order_amount' => (int)$this->accept_amt ?? 0,
            'create_date' => $this->create_date ?? null,
            'store_id' => $this->store_id ?? null,
            'title' => $this->title ?? null,

        ];
    }

}
