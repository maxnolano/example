<?php


namespace App\Presenters\v1;


use App\Helper\ImageHelper;
use App\Presenters\BasePresenter;

class UserPresenter extends BasePresenter
{
    /**
     * @todo уточнить про поле plate_number
     * получение профиля пользователя
     * @return array
     */
    public function getProfile() : array
    {
        $thumbnail = ImageHelper::get_thumbnail($this->thumbnail, 'full');

        return [
            'display_name' => $this->display_name ?? null,
            'first_name' => $this->first_name ?? null,
            'last_name' => $this->last_name ?? null,
            'phone' => $this->phone ?? null,
            'email' => $this->email ?? null,
            'vehicle_model_id' => $this->vehicle_model_id ?? 0,
            'vehicle_company_id' => $this->vehicle_company_id ?? 0,
            'city_id' => $this->city_id ?? 0,
            'vehicle_plate_number' => $this->vehicle_plate_number ?? null,
            'iin' => $this->iin ?? null,
            'thumbnail' =>  $thumbnail ?? null
        ];
    }
}