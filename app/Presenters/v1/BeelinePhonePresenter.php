<?php


namespace App\Presenters\v1;


use App\Presenters\BasePresenter;

class BeelinePhonePresenter extends BasePresenter
{
    /**
     * получение товара при выводе в листе
     * @return array
     */
    public function getBeelinePhoneElementInList() : array
    {
        return [
            'phone' => $this->phone_number,
        ];
    }
}