<?php


namespace App\Presenters\v1;


use App\Presenters\BasePresenter;
use App\Repositories\Interfaces\VehicleCompanyRepositoryInterface;
use App\Repositories\VehicleCompanyRepository;
use Illuminate\Database\Eloquent\Model;

class VehiclePresenter extends BasePresenter
{
    // репозиторий для работы с транспортом
    private VehicleCompanyRepositoryInterface $vehicleCompanyRepository;

    public function __construct(Model $model)
    {
        $this->vehicleCompanyRepository = new VehicleCompanyRepository();

        parent::__construct($model);

    }

    /**
     * получение элемента в листе
     */
    public function getVehicleElementInList() : array
    {
        return [
            'company_id' => $this->company_id,
            'company_name' => $this->company_name ?? null,
            'date_added' => $this->date_added ?? null,
            'date_modified' => $this->date_modified ?? null,
            'model_list' => $this->vehicleCompanyRepository->getVehicleModelByCompany($this->company_id) ?? [],
        ];
    }
}