<?php


namespace App\Presenters\v1;


use App\Presenters\BasePresenter;
use App\Repositories\FaqRepository;
use App\Repositories\Interfaces\VehicleCompanyRepositoryInterface;
use App\Repositories\VehicleCompanyRepository;
use Illuminate\Database\Eloquent\Model;

class FaqPresenter extends BasePresenter
{
    // репозиторий для работы с транспортом
    private $faqRepository;

    public function __construct(Model $model)
    {
        $this->faqRepository = new FaqRepository();

        parent::__construct($model);

    }

    /**
     * получение элемента в листе
     */
    public function getFaqElementInList() : array
    {
        return [
            'id' => $this->id,
            'question' => $this->question,
            'answer' => $this->answer,
        ];
    }
}