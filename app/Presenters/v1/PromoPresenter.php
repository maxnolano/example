<?php


namespace App\Presenters\v1;


use App\Helper\ImageHelper;
use App\Presenters\BasePresenter;

class PromoPresenter extends BasePresenter
{
    public function item() : array
    {
        $thumbnail = ImageHelper::get_thumbnail($this->thumbnail, 'small');

        return [
            'id' => $this->id,
            'title' => $this->title,
            'thumbnail' =>  $thumbnail ?? null
        ];
    }

    public function details() : array
    {
        $thumbnail = ImageHelper::get_thumbnail($this->thumbnail, 'full');

        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'expire_date' => $this->expire_date,
            'thumbnail' =>  $thumbnail ?? null,
            'store_id' => $this->store_id
        ];
    }
}