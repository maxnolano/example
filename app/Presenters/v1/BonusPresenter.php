<?php


namespace App\Presenters\v1;


use App\Presenters\BasePresenter;

class BonusPresenter extends BasePresenter
{

    public function item(): array
    {
        return [
            'bonus_amount' => $this->total_amount ?? 0,
        ];
    }

}