<?php


namespace App\Presenters\v1;


use App\Presenters\BasePresenter;

class CardPresenter extends BasePresenter
{

    public function item(): array
    {
        return [
            'id' => $this->id ?? null,
            'description' => $this->description ?? null,
            'holder_name' => $this->holder_name ?? null,
            'card_hash' => $this->card_hash ?? null,
            'exp_month' => $this->exp_month ?? null,
            'exp_year' => $this->exp_year ?? null,
            'status' => $this->status ?? null,
            'is_default' => $this->is_default ?? null,
        ];
    }

}