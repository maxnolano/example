<?php


namespace App\Presenters\v1;


use App\Helper\ImageHelper;
use App\Presenters\BasePresenter;

class GroupMemberPresenter extends BasePresenter
{
    /**
     * @todo уточнить про поле plate_number
     * получение профиля пользователя
     * @return array
     */
    public function getItem() : array
    {
        $thumbnail = ImageHelper::get_thumbnail($this->thumbnail, 'full');
        /*
         * crm_users_group.is_owner', 'u.display_name', 'u.id', 'u.phone',
            'crm_users_group.invite_status', 'u.thumbnail
         */

        return [
            'display_name' => $this->display_name ?? null,
            'is_owner' => $this->is_owner ?? null,
            'id' => $this->id ?? null,
            'phone' => $this->phone ?? null,
            'invite_status' => $this->invite_status ?? null,
            'thumbnail' =>  $thumbnail ?? null
        ];
    }
}