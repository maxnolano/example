<?php


namespace App\Presenters\v1;


use App\Presenters\BasePresenter;
use App\Repositories\CityRepository;
use Illuminate\Database\Eloquent\Model;

class CityPresenter extends BasePresenter
{
    // репозиторий для работы с транспортом
    private $cityRepository;

    public function __construct(Model $model)
    {
        $this->cityRepository = new CityRepository();

        parent::__construct($model);

    }

    /**
     * получение элемента в листе
     */
    public function getCityElementInList($appLang) : array
    {
        if ($appLang == 'ru' || $appLang == '' ) {
            return [
                    'id' => $this->id,
                    'title' => $this->rus_title
                ];

        } else {
            return [
                'id' => $this->id,
                'title' => $this->title
            ];
        }
    }
}