<?php


namespace App\Presenters\v1;


use App\Presenters\BasePresenter;

class ProductAmountBalancePresenter   extends BasePresenter
{

    /**
     * получение товара при выводе в листе
     * @return array
     */
    public function getProductBalanceElementInList() : array
    {
        return [
            'product_id' => (int)$this->spid,
            'store_product_price' => (float)$this->store_product_price,
            'title' => $this->title,
            'money_available' => (float)$this->amount_balance
        ];
    }
}