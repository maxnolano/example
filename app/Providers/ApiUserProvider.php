<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Auth\UserProvider;

class ApiUserProvider implements UserProvider
{
    /**
     * The Eloquent user model.
     *
     * @var string
     */
    protected $userModel;

    /**
     * The Eloquent token model.
     *
     * @var string
     */
    protected $tokenModel;

    /**
     * Create a new database user provider.
     *
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @param  string  $userModel
     * @param  string  $tokenModel
     * @return void
     */
    public function __construct($userModel, $tokenModel)
    {
        $this->userModel = $userModel;
        $this->tokenModel = $tokenModel;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $model = $this->createUserModel();

        return $this->newUserModelQuery($model)
            ->where($model->getAuthIdentifierName(), $identifier)
            ->first();
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        throw new \Exception('Метод retrieveByToken не используется в API');
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable|\Illuminate\Database\Eloquent\Model  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(UserContract $user, $token)
    {
        throw new \Exception('Метод updateRememberToken не используется в API');
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials['token'])) {
            return;
        }

        $token = $this->newTokenModelQuery()
            ->where('token', trim($credentials['token']))
            ->where('expired_date', '>=', time())
            ->where('disabled', 0)
            ->where('deleted', 0)
            ->first();

        if (empty($token)) {
            return null;
        }

        return $token->user;
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        throw new \Exception('Метод validateCredentials не используется в API');
    }

    /**
     * Get a new query builder for the model instance.
     *
     * @param  \Illuminate\Database\Eloquent\Model|null  $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function newUserModelQuery($model = null)
    {
        return is_null($model)
            ? $this->createUserModel()->newQuery()
            : $model->newQuery();
    }

    /**
     * Create a new instance of the model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createUserModel()
    {
        $class = '\\' . ltrim($this->userModel, '\\');

        return new $class;
    }


    /**
     * Get a new query builder for the model instance.
     *
     * @param  \Illuminate\Database\Eloquent\Model|null  $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function newTokenModelQuery($model = null)
    {
        return is_null($model)
            ? $this->createTokenModel()->newQuery()
            : $model->newQuery();
    }

    /**
     * Create a new instance of the model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createTokenModel()
    {
        $class = '\\' . ltrim($this->tokenModel, '\\');

        return new $class;
    }
}
