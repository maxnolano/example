<?php


namespace App\Providers;


use App\Repositories\ApiTokenRepository;
use App\Repositories\Interfaces\ApiTokenRepositoryInterface;
use App\Repositories\Interfaces\IpLogRepositoryInterface;
use App\Repositories\Interfaces\OtpRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\IpLogRepository;
use App\Repositories\OtpRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            IpLogRepositoryInterface::class,
            IpLogRepository::class
        );

        $this->app->bind(
            OtpRepositoryInterface::class,
            OtpRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );

        $this->app->bind(
            ApiTokenRepositoryInterface::class,
            ApiTokenRepository::class
        );



    }
}