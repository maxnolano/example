<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Providers\Guard\JwtGuard;

class AuthServiceProvider extends ServiceProvider
{

    public function __construct($app)
    {
        parent::__construct($app);
    }

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Auth::extend('jwt', function ($app, $name, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...
            return new JwtGuard(
                new ApiUserProvider(
                    'App\Models\User',
                    'App\Models\ApiToken'
                ),
                $app['request']
            );
        });
    }
}
