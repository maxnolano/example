<?php

return [

    // урл сервера где хранятся картинки
    'upload_host_url' => env('UPLOAD_HOST_URL', null),

    // Системы управления АЗС
    'stations_providers' => [
        'SNCard' => [
            'url' => env('SNCARD_URL')
        ],
        'Topline' => [
            'url' => env('TOPLINE_URL')
        ],
        'Servio' => [],
        'Masterpos' => [],
        'RetalixA' => [],
        'RetalixB' => [],
        'Default' => [],


    ]
];
